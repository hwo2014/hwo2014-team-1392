/*jslint white: true */
/*jslint node:true*/

var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

var joinType = 'solo';
if(process.argv.length>=7){
	joinType = process.argv[6];
}
var cars = [];
var myCar = {throttle:0, state:'qualifying', turbo: 1, turboTimer: 0, turboAvailable:false};
var race;

var enginePower = 10;
var acceleration = 0.02;
var deceleration = 0.02;
var divisor = 200;

console.log("I'm", botName, "and connect to", serverHost , ":" , serverPort);

var send;
var client = net.connect(serverPort, serverHost, function() {
	if(joinType==='solo'){
		return send({
			msgType: "join",
			data: {
				name: botName,
				key: botKey
			}
		});
	}else{
		return send({
			msgType: joinType,
			data: {
				botId:{
					name: botName + '-' + Math.random().toString(36).substr(2, 2),
					key: botKey
				},
				trackName: "imola",	//keimola germany usa france elaeintarha imola england suzuka
				//password: "foo",
				carCount: 1
			}
		});
	}
});
client.setNoDelay(true);

function send(json) {
	//console.log('out >',JSON.stringify(json));
	client.write(JSON.stringify(json));
	return client.write('\n');
}

var jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data){
	//console.log('in  <',JSON.stringify(data));
	switch(data.msgType){
		case 'join':
		case 'joinRace':
		case 'createRace':
			console.log('Joined');
			break;
		case 'yourCar':
			myCar.color = data.data.color;
			console.log('Car color: '+myCar.color);
			break;
		case 'gameInit':
			/*
{"msgType": "gameInit", "data": {
  "race": {
    "track": {
      "id": "indianapolis",
      "name": "Indianapolis",
      "pieces": [
        {
          "length": 100.0
        },
        {
          "length": 100.0,
          "switch": true
        },
        {
          "radius": 200,
          "angle": 22.5
        }
      ],
      "lanes": [
        {
          "distanceFromCenter": -20,
          "index": 0
        },
        {
          "distanceFromCenter": 0,
          "index": 1
        },
        {
          "distanceFromCenter": 20,
          "index": 2
        }
      ],
      "startingPoint": {
        "position": {
          "x": -340.0,
          "y": -96.0
        },
        "angle": 90.0
      }
    },
    "cars": [
      {
        "id": {
          "name": "Schumacher",
          "color": "red"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      },
      {
        "id": {
          "name": "Rosberg",
          "color": "blue"
        },
        "dimensions": {
          "length": 40.0,
          "width": 20.0,
          "guideFlagPosition": 10.0
        }
      }
    ],
    "raceSession": {
      "laps": 3,
      "maxLapTimeMs": 30000,
      "quickRace": true
    }
  }
}}	*/
			race = data.data.race;
			race.track.pieces.forEach(function(piece){
				piece.lane = [];
				if(piece.radius===undefined){   //recta
					for(var i=0;i<race.track.lanes.length;i++)
						piece.lane[i] = {length: piece.length, angle: 0};
				}else{  //curva

					for(var i=0;i<race.track.lanes.length;i++){
						var radius = piece.radius + (piece.angle>0?-race.track.lanes[i].distanceFromCenter:
													 race.track.lanes[i].distanceFromCenter);
						var length = Math.abs(toRadians(piece.angle)*radius);
						piece.lane[i] = {length: length, radius: radius, angle: piece.angle};
					}
				}
			});
			console.log(race.track.name,JSON.stringify(race));
			break;
		case 'gameStart':
			/*	{"msgType": "gameStart", "data": null, "gameTick": 0}	*/
			console.log('Race started');
			onCarPositions(data);
			break;
		case 'gameEnd':
			console.log('Race ended');
			myCar.state = 'ready';
			break;
		case 'crash':
			/*	{"msgType": "crash", "data": {
				  "name": "Rosberg",
				  "color": "blue"
				}, "gameId": "OIUHGERJWEOI", "gameTick": 3}*/
			if(data.data.color===myCar.color){
				console.log('crashed: ' +
							myCar.Position.piecePosition.pieceIndex + ' r' +
							race.track.pieces[myCar.Position.piecePosition.pieceIndex].
							lane[myCar.Position.piecePosition.lane.startLaneIndex].radius + ' @ ' +
							myCar.speed + ' ' + myCar.throttle + ' ' + myCar.Position.angle);
				myCar.crashedTick = data.gameTick;
				myCar.state = 'crashed';
			}
			//Re-calculate divisor
			var newDivisor = race.track.pieces[cars[data.data.color].Position.piecePosition.pieceIndex].
							lane[cars[data.data.color].Position.piecePosition.lane.startLaneIndex].radius
							/(cars[data.data.color].speed*0.1-0.3);
			if(newDivisor>divisor){
				console.log('New divisor: '+newDivisor+', old: '+divisor);
				divisor = newDivisor;
			}
			break;
		case 'spawn':
			/*	{"msgType": "spawn", "data": {
				  "name": "Rosberg",
				  "color": "blue"
				}, "gameId": "OIUHGERJWEOI", "gameTick": 150} */
			if(data.data.color===myCar.color){
				console.log('spawned after ' + (data.gameTick - myCar.crashedTick));
				myCar.state = 'ready';
				myCar.turboAvailable = false;
			}
			break;
		case 'finish':
			/*	{"msgType": "finish", "data": {
				  "name": "Schumacher",
				  "color": "red"
				}, "gameId": "OIUHGERJWEOI", "gameTick": 2345}*/
			if(data.data.color===myCar.color){
				console.log('finish ' + JSON.stringify(data));
				myCar.state = 'finish';
			}else
				console.log('finish '+data.name+' '+data.color);
			break;
		case 'turboAvailable':
			myCar.turboAvailable = true;
			myCar.turboFactor = data.data.turboFactor;
			console.log('turbo available');
			/*enginePower *= myCar.turbo;
			myCar.turboTimer = 30;
			send({
				msgType: "turbo",
				data: "Ay Caramba!"
			});*/
			break;
		case 'carPositions':
			data.data.forEach(function(data){
				if(cars[data.id.color]===undefined)
					cars[data.id.color] = {};
				cars[data.id.color].PreviousPosition = cars[data.id.color].Position;
				cars[data.id.color].Position = data;
				if(cars[data.id.color].PreviousPosition===undefined){
					cars[data.id.color].PreviousPosition = cars[data.id.color].Position;
				}
				if(cars[data.id.color].Position.piecePosition.pieceIndex===cars[data.id.color].PreviousPosition.piecePosition.pieceIndex)
					cars[data.id.color].speed = cars[data.id.color].Position.piecePosition.inPieceDistance - cars[data.id.color].PreviousPosition.piecePosition.inPieceDistance;
				else
					cars[data.id.color].speed = cars[data.id.color].Position.piecePosition.inPieceDistance + race.track.pieces[cars[data.id.color].PreviousPosition.piecePosition.pieceIndex].lane[cars[data.id.color].PreviousPosition.piecePosition.lane.endLaneIndex].length - cars[data.id.color].PreviousPosition.piecePosition.inPieceDistance;
				/*if(data.id.color === myCar.color){
					myCar.PreviousPosition = myCar.Position;
					myCar.Position = data;
					if(myCar.PreviousPosition===undefined){
						myCar.PreviousPosition = myCar.Position;
					}
				}*/
			});
			myCar.PreviousPosition = cars[myCar.color].PreviousPosition;
			myCar.Position = cars[myCar.color].Position;
			myCar.speed = cars[myCar.color].speed;
			
			if(myCar.turbo>1){
				myCar.turboTimer--;
				if(myCar.turboTimer<=0){
					enginePower /= myCar.turbo;
					myCar.turbo = 1;
				}
			}
			if(data.gameTick)
				onCarPositions(data);
			break;
		default:
			console.log('unhandled msgType: '+data.msgType+' '+JSON.stringify(data));
	}
});

/*	carPositions Example
{"msgType": "carPositions", "data": [
  {
	"id": {
	  "name": "Schumacher",
	  "color": "red"
	},
	"angle": 0.0,
	"piecePosition": {
	  "pieceIndex": 0,
	  "inPieceDistance": 0.0,
	  "lane": {
		"startLaneIndex": 0,
		"endLaneIndex": 0
	  },
	  "lap": 0
	}
  },
  {
	"id": {
	  "name": "Rosberg",
	  "color": "blue"
	},
	"angle": 45.0,
	"piecePosition": {
	  "pieceIndex": 0,
	  "inPieceDistance": 20.0,
	  "lane": {
		"startLaneIndex": 1,
		"endLaneIndex": 1
	  },
	  "lap": 0
	}
  }
], "gameId": "OIUHGERJWEOI", "gameTick": 0}	*/
function onCarPositions(data){
	switch(myCar.state){
		case 'qualifying':			
			if(data.gameTick===0)
				myCar.throttle = 1;
			if(data.gameTick===1)
				enginePower = myCar.speed;
			if(data.gameTick===2)
				myCar.throttle = 0,
				acceleration = 1-(myCar.speed-enginePower)/enginePower,
				enginePower *= 1/acceleration;
			if(data.gameTick===3)
				deceleration = [myCar.speed];
			if(data.gameTick===4)
				deceleration[1] = myCar.speed;
			if(data.gameTick===5)
				deceleration = 1-(myCar.speed-deceleration[1])/(deceleration[1]-deceleration[0]),
				myCar.state = 'qualifying2',
				console.log('acc',acceleration,'dec',deceleration,'engine',enginePower);
			break;
		case 'qualifying2':
			//aqui calcular angulos
			myCar.state = 'ready';
		case 'ready':
			if(myCar.turbo===1){
				if(myCar.turboAvailable &&
				   race.track.pieces[myCar.Position.piecePosition.pieceIndex].radius===undefined &&
				   race.track.pieces[(myCar.Position.piecePosition.pieceIndex+1)%race.track.pieces.length].radius===undefined &&
				   race.track.pieces[(myCar.Position.piecePosition.pieceIndex+2)%race.track.pieces.length].radius===undefined &&
				   race.track.pieces[(myCar.Position.piecePosition.pieceIndex+3)%race.track.pieces.length].radius===undefined
				  ){
					console.log('turbo activado');
					myCar.turboAvailable = false;
					myCar.turbo = myCar.turboFactor;
					enginePower *= myCar.turboFactor;
					myCar.turboTimer = 30;
					return send({
						msgType: "turbo",
						data: "Ay Caramba!"
					});
				}
			}
			
			if(myCar.Position.piecePosition.pieceIndex!==myCar.PreviousPosition.piecePosition.pieceIndex && 
			  race.track.pieces[(myCar.Position.piecePosition.pieceIndex+1)%race.track.pieces.length].switch===true){
				var path = choosePath();
				if(path<myCar.Position.piecePosition.lane.endLaneIndex)
					return send({msgType: "switchLane", data: "Left"});
				if(path>myCar.Position.piecePosition.lane.endLaneIndex)
					return send({msgType: "switchLane", data: "Right"});
			}
			
			var maxSpeed = enginePower;
			for(var i = myCar.Position.piecePosition.pieceIndex; i<myCar.Position.piecePosition.pieceIndex+5; i++){
				var maxSpeedTemp = getMaxSpeed(i,myCar.Position.piecePosition.lane.endLaneIndex);
				if(maxSpeedTemp < maxSpeed && race.track.pieces[i%race.track.pieces.length].radius!==undefined){
					var distanceToChange = ((myCar.throttle*enginePower)-myCar.speed) *
						(myCar.throttle*enginePower>myCar.speed?acceleration:deceleration) + getSpeedChangeDistance(maxSpeedTemp);
						if(distanceToChange>getDistanceToPiece(i))
							maxSpeed = maxSpeedTemp;
				}
			}
			
			//emergency brake
			if(Math.abs(myCar.Position.angle)+(Math.abs(myCar.Position.angle)-Math.abs(myCar.PreviousPosition.angle))*5>60
			  && maxSpeed>myCar.speed*0.8){
				maxSpeed = myCar.speed*0.8;
				console.log('emergency brake');
			}
			
			changeSpeed(maxSpeed);
			
			if(myCar.throttle>1)
				myCar.throttle = 1;
			if(myCar.throttle<0)
				myCar.throttle = 0;

			break;
		case 'crashed':
		case 'finish':
			return send({
					msgType: 'ping',
					data:{}
				});
			break;
		default:
			console.log('unhandled state: ' + myCar.state);
			return send({
					msgType: 'ping',
					data:{}
				});
	}
	console.log(data.gameTick +
			' ' + myCar.speed.toFixed(10) +
			' ' + myCar.throttle.toFixed(10) +
			' ' + myCar.Position.angle.toFixed(10) +
			' ' + myCar.Position.piecePosition.pieceIndex +
			' ' + (race.track.pieces[myCar.Position.piecePosition.pieceIndex].angle<0?'-':'') +
			race.track.pieces[myCar.Position.piecePosition.pieceIndex].
			lane[myCar.Position.piecePosition.lane.startLaneIndex].radius +
			' ' + myCar.Position.piecePosition.lane.startLaneIndex);
	send({
		msgType: "throttle",
		data: myCar.throttle,
		gameTick: data.gameTick
	});
}

function mySpeedCloseTo(speed){
	return myCar.speed >= speed*0.99 && myCar.speed <= speed*1.01;
}

function getDistanceToPiece(piece){
	if(piece<myCar.Position.piecePosition.pieceIndex)
		piece += race.track.pieces.length;
	var distance = race.track.pieces[myCar.Position.piecePosition.pieceIndex].lane[myCar.Position.piecePosition.lane.endLaneIndex].length - myCar.Position.piecePosition.inPieceDistance;
	for(var i = myCar.Position.piecePosition.pieceIndex+1; i<piece; i++)
		distance += race.track.pieces[i%race.track.pieces.length].lane[myCar.Position.piecePosition.lane.endLaneIndex].length;
	return distance;
}

function getMaxSpeed(piece, lane){
	if(race.track.pieces[piece%race.track.pieces.length].radius===undefined)
		return enginePower;
	return 10 * (0.3 + race.track.pieces[piece%race.track.pieces.length].lane[lane].radius/divisor);
}

function getSpeedChangeDistance(newSpeed){
	var distance = 0, speed = myCar.speed, tick = 0, throttle = myCar.throttle;
	var change = newSpeed>speed?acceleration:deceleration;
	while(speed < newSpeed*0.99 || speed > newSpeed*1.01){
		throttle = ((newSpeed-speed)/change+speed)/enginePower;
		if(throttle>1) throttle = 1;
		if(throttle<0) throttle = 0;
		//console.log(speed+' -> '+newSpeed+' = '+distance+' , '+throttle);
		speed += ((throttle*enginePower)-speed)*change;
		distance += speed;
		tick++;
	}
	return distance;
}

function changeSpeed(newSpeed){
	var change = newSpeed>myCar.speed?acceleration:deceleration;
	myCar.throttle = ((newSpeed-myCar.speed)/change+myCar.speed)/enginePower;
}

function choosePath(){
	var lanes = [];
	for(var i = myCar.Position.piecePosition.lane.endLaneIndex-1;i<=myCar.Position.piecePosition.lane.endLaneIndex+1;i++){
		if(i>=0 && i<race.track.lanes.length){
			var j = myCar.Position.piecePosition.pieceIndex+2;
			lanes[i] = i===myCar.Position.piecePosition.lane.endLaneIndex?-5:0;
			while(race.track.pieces[j%race.track.pieces.length].switch!==true){
				lanes[i] += race.track.pieces[j%race.track.pieces.length].lane[i].length;
				j++;
			}
		}
	}
	//TODO check for obstacles
	return lanes.indexOf(Math.min.apply(Math, lanes));
}

function toRadians(degrees) {
	return degrees * Math.PI / 180;
};

jsonStream.on('error', function() {
	return console.log("disconnected");
});